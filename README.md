# CoDev ![PowerShell Gallery](https://img.shields.io/powershellgallery/p/Az?color=gree&logo=linux&style=plastic)


:star: Star us on GitLab — it motivates us a lot!

## About

It is just a compiler for compiling a language called CoDev which is created in C language. There is an example of the code that should be valid and be able to be compiled in `example`. As the progress is made in this project, the readme file will be updated accordingly. So stay tuned for more...

> [!NOTE]
> Shell commands shown assume a working directory of this repository.

## Usage

Run the executable from a shell with a path to some source code as the only argument. Currently, we print out the furthest progress we are able to make. Eventually, we will output compiled source code.

## Dependecies:

- [CMake >= 3.14](https://cmake.org/)
- Any C Compiler (We like [GCC](https://gcc.gnu.org/))

## Building

First, generate a build tree using CMake.
```shell
cmake -G Ninja -B bld -DCMAKE_BUILD_TYPE=Release
```

Finally, build an executable from the build tree.
```shell
cmake --build bld
```
## Run

You can run the the executable as:-
```shell
bld/cd example
```

## Support

If you encounter any issue while using this compiler, feel free to open an issue or dm me on discord @Prateekshit Jaiswal#4614

## License 

The files and scripts in this repository are licensed under the [GNU GPLv3 License](https://gitlab.com/prateekshitjaiswal73/dotfiles/-/blob/main/LICENSE) .


